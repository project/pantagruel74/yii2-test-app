<?php
$ds = DIRECTORY_SEPARATOR;
$config = \Pantagruel74\Yii2TestApp\YiiDefaultConfig::getBase(dirname(__DIR__));
$config['components']['testComponent'] = [
    'class' => \Pantagruel74\Yii2TestAppStubs\TestComponent::class,
    'testVar' => 'fiasf',
];

return $config;