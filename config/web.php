<?php
$ds = DIRECTORY_SEPARATOR;

$config = \Pantagruel74\Yii2TestApp\YiiDefaultConfig::getWeb(dirname(__DIR__));
$config['components']['testComponent'] = [
    'class' => \Pantagruel74\Yii2TestAppStubs\TestComponent::class,
    'testVar' => 'fiasf',
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];
}

return $config;
