<?php

namespace Pantagruel74\Yii2TestAppTestHelpers;

use Pantagruel74\Yii2Loader\Yii2Loader;
use Pantagruel74\Yii2TestApp\BaseTestApplication;
use PHPUnit\Framework\TestCase;

abstract class AbstractBaseTest extends TestCase
{
    abstract protected function getConfig(): array;

    abstract protected function testScenario(): void;

    /**
     * @param string|null $name
     * @param array $data
     * @param $dataName
     */
    public function __construct(?string $name = null, array $data = [], $dataName = '')
    {
        Yii2Loader::load();
        parent::__construct($name, $data, $dataName);
    }

    /**
     * @return void
     * @throws \yii\base\InvalidConfigException
     */
    public function testController(): void
    {
        $config = $this->getConfig();
        $app = new BaseTestApplication($config);
        $this->testScenario();
    }
}