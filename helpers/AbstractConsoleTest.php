<?php

namespace Pantagruel74\Yii2TestAppTestHelpers;

use Pantagruel74\Yii2Loader\Yii2Loader;
use Pantagruel74\Yii2TestApp\ConsoleTestApplication;
use Pantagruel74\Yii2TestApp\TestRequest;
use PHPUnit\Framework\TestCase;
use yii\console\Response;

abstract class AbstractConsoleTest extends TestCase
{
    abstract protected function getConfig(): array;

    abstract protected function awaitConsoleOutput(): ?string;

    abstract protected function getRoute(): string;

    abstract protected function getParams(): array;

    abstract protected function assertAfterRunState(): void;

    abstract protected function preRunScript(): void;

    /**
     * @param string|null $name
     * @param array $data
     * @param $dataName
     */
    public function __construct(?string $name = null, array $data = [], $dataName = '')
    {
        Yii2Loader::load();
        parent::__construct($name, $data, $dataName);
    }

    /**
     * @return void
     * @throws \yii\base\ExitException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\base\InvalidRouteException
     */
    public function testController(): void
    {
        $config = $this->getConfig();
        $config['components']['request'] = new TestRequest([
            'route' => $this->getRoute(),
            'params' => $this->getParams(),
        ]);
        $config['components']['response'] = [
            'class' => Response::class,
        ];
        $app = new ConsoleTestApplication($config);
        $this->preRunScript();
        $app->run();

        if(!is_null($this->awaitConsoleOutput())) {
            $this->expectOutputString($this->awaitConsoleOutput());
        }
        $this->assertAfterRunState();
    }
}