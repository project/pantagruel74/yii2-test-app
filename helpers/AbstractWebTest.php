<?php

namespace Pantagruel74\Yii2TestAppTestHelpers;

use Pantagruel74\Yii2TestApp\TestRequest;
use Pantagruel74\Yii2TestApp\WebTestApplication;
use PHPUnit\Framework\TestCase;
use yii\web\Response;

abstract class AbstractWebTest extends TestCase
{
    abstract protected function getConfig(): array;

    abstract protected function getRoute(): string;

    abstract protected function getParams(): array;

    abstract protected function assertAfterRunState(): void;

    abstract protected function assertOutput(string $content): void;

    abstract protected function preRunScript(): void;

    /**
     * @return void
     * @throws \yii\base\ExitException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\base\InvalidRouteException
     */
    public function testController(): void
    {
        $config = $this->getConfig();
        $config['components']['request'] = new TestRequest([
            'route' => $this->getRoute(),
            'params' => $this->getParams(),
        ]);
        $config['components']['response'] = [
            'class' => Response::class,
        ];
        $app = new WebTestApplication($config);
        $this->preRunScript();
        $app->run();
        $response = $app->resultResponse;

        $this->assertOutput($response->exitStatus);
        $this->assertAfterRunState();
    }
}