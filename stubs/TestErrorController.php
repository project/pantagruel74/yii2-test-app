<?php

namespace Pantagruel74\Yii2TestAppStubs;

use yii\web\Controller;

class TestErrorController extends Controller
{
    public $enableCsrfValidation = false;

    /**
     * @return mixed
     * @throws \ErrorException
     */
    public function actionTest()
    {
        throw new \ErrorException('!!');
    }
}