<?php

namespace Pantagruel74\Yii2TestAppStubs;

use yii\web\Controller;

class TestController extends Controller
{
    public $enableCsrfValidation = false;

    public function actionTest()
    {
        echo 'console-test-successfull!';
        return 'web-console-test-successfull!';
    }
}