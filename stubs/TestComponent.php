<?php

namespace Pantagruel74\Yii2TestAppStubs;

use yii\base\Model;

class TestComponent extends Model
{
    public string $testVar;

    public function getTestVar(): string
    {
        return $this->testVar;
    }
}