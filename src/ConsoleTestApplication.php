<?php

namespace Pantagruel74\Yii2TestApp;

use yii\console\Application;

class ConsoleTestApplication extends Application
{
    use TestApplicationTrait;

}