<?php

namespace Pantagruel74\Yii2TestApp;

class YiiDefaultConfig
{
    public static function getBase(string $basePath): array
    {
        return [
            'id' => 'basic',
            'basePath' => $basePath,
            'bootstrap' => ['log'],
            'aliases' => [
                '@bower' => '@vendor/bower-asset',
                '@npm'   => '@vendor/npm-asset',
            ],
            'components' => [
                'log' => [
                    'traceLevel' => YII_DEBUG ? 3 : 0,
                    'targets' => [
                        [
                            'class' => 'yii\log\FileTarget',
                            'levels' => ['error', 'warning'],
                        ],
                    ],
                ],
            ],
        ];
    }

    /**
     * @param string $basePath
     * @return array
     */
    public static function getWeb(string $basePath): array
    {
        $config = self::getBase($basePath);
        $config['components']['urlManager'] = [
            'baseUrl' => '/',
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
            ],
        ];
        $config['components']['request'] = [
            'baseUrl' => '',
            'cookieValidationKey' => '00i-Ji17czC6gOrzQX3bxFOcoi-G0ays',
            'parsers' => [
                'application/json' => [
                    'class' => \yii\web\JsonParser::class,
                    'asArray' => true,
                ],
            ],
        ];
        $config['components']['user'] = [
            'identityClass' => \yii\web\User::class,
            'enableAutoLogin' => true,
        ];
        $config['params'] = [
            'registeredHomepage' => '/index',
            'guestHomepage' => '/login',
            'startPagesForRoles' => [
                '?' => '/login',
                '@' => '/index'
            ],
        ];
        return $config;
    }
}