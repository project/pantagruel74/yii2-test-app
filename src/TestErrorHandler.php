<?php

namespace Pantagruel74\Yii2TestApp;

use yii\base\ErrorHandler;

class TestErrorHandler extends ErrorHandler
{
    /**
     * @param $exception
     * @return mixed
     */
    protected function renderException($exception)
    {
        echo $exception;
    }
}