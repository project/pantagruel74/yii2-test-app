<?php

namespace Pantagruel74\Yii2TestApp;

use yii\web\Application;

class WebTestApplication extends Application
{
    use TestApplicationTrait;
}