<?php

namespace Pantagruel74\Yii2TestApp;

use yii\base\Application;

class BaseTestApplication extends Application
{
    use TestApplicationTrait;
}