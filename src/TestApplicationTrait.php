<?php

namespace Pantagruel74\Yii2TestApp;

use yii\base\Application;
use yii\base\ExitException;
use yii\base\Response;

trait TestApplicationTrait
{
    /**
     * @var int the current application state during a request handling life cycle.
     * This property is managed by the application. Do not modify this property.
     */
    public $state;
    
    public ?Response $resultResponse = null;

    /**
     * @var string the requested route
     */
    public $requestedRoute;

    abstract public function getRequest();

    /**
     * @return \string[][]
     */
    public function coreComponents(): array
    {
        /* @var Application $this */
        $components = [
            'log' => ['class' => 'yii\log\Dispatcher'],
            'view' => ['class' => 'yii\web\View'],
            'formatter' => ['class' => 'yii\i18n\Formatter'],
            'i18n' => ['class' => 'yii\i18n\I18N'],
            'urlManager' => ['class' => 'yii\web\UrlManager'],
            'assetManager' => ['class' => 'yii\web\AssetManager'],
            'security' => ['class' => 'yii\base\Security'],
            'errorHandler' => ['class' => TestErrorHandler::class],
        ];
        if (class_exists('yii\swiftmailer\Mailer')) {
            $components['mailer'] = ['class' => 'yii\swiftmailer\Mailer'];
        }
        return $components;
    }

    /**
     * @param TestRequest $request
     * @return Response
     * @throws \yii\base\InvalidRouteException
     */
    public function handleRequest($request)
    {
        /* @var Application $this */
        list($route, $params) = $request->resolve();
        $this->requestedRoute = $route;
        $result = $this->runAction($route, $params);
        if ($result instanceof Response) {
            return $result;
        }

        $response = $this->getResponse();
        $response->exitStatus = $result;

        return $response;
    }

    /**
     * Runs the application.
     * This is the main entrance of an application.
     * @return int the exit status (0 means normal, non-zero values mean abnormal)
     * @throws \yii\base\ExitException
     * @throws \yii\base\InvalidRouteException
     */
    public function run()
    {
        /* @var Application $this */
        try {
            $this->state = Application::STATE_BEFORE_REQUEST;
            $this->trigger(Application::EVENT_BEFORE_REQUEST);

            $this->state = Application::STATE_HANDLING_REQUEST;
            $response = $this->handleRequest($this->getRequest());

            $this->state = Application::STATE_AFTER_REQUEST;
            $this->trigger(Application::EVENT_AFTER_REQUEST);

            $this->state = Application::STATE_SENDING_RESPONSE;
            $this->resultResponse = $response;

            $this->state = Application::STATE_END;

            return $response->exitStatus;
        } catch (ExitException $e) {
            $this->end($e->statusCode, isset($response) ? $response : null);
            return $e->statusCode;
        }
    }
}