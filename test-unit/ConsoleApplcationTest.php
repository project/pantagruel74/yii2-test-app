<?php

namespace Pantagruel74\Yii2TestAppTestUnit;

use Pantagruel74\Yii2Loader\Yii2Loader;
use Pantagruel74\Yii2TestApp\ConsoleTestApplication;
use Pantagruel74\Yii2TestApp\TestRequest;
use Pantagruel74\Yii2TestAppTestHelpers\AbstractConsoleTest;
use PHPUnit\Framework\TestCase;
use yii\console\Response;

class ConsoleApplcationTest extends AbstractConsoleTest
{
    /**
     * @return array
     */
    protected function getConfig(): array
    {
        $ds = DIRECTORY_SEPARATOR;
        $config = include dirname(__DIR__) . $ds . 'config' . $ds . 'console.php';
        $config['controllerNamespace'] = 'Pantagruel74\Yii2TestAppStubs';
        return $config;
    }

    /**
     * @return string|null
     */
    protected function awaitConsoleOutput(): ?string
    {
        return 'console-test-successfull!';
    }

    protected function getRoute(): string
    {
        return 'test/test';
    }

    protected function getParams(): array
    {
        return [];
    }

    protected function assertAfterRunState(): void
    {

    }

    protected function preRunScript(): void
    {
    }
}