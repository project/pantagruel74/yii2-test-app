<?php

namespace Pantagruel74\Yii2TestAppTestUnit;

use Pantagruel74\Yii2TestAppTestHelpers\AbstractBaseTest;

class BaseApplicationTest extends AbstractBaseTest
{
    /**
     * @return array
     */
    protected function getConfig(): array
    {
        $ds = DIRECTORY_SEPARATOR;
        $config = include dirname(__DIR__) . $ds . 'config' . $ds . 'base.php';
        return $config;
    }

    protected function testScenario(): void
    {
        $result = \Yii::$app->testComponent->getTestVar();
        $this->assertEquals('fiasf', $result);
    }
}