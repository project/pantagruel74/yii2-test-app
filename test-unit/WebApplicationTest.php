<?php

namespace Pantagruel74\Yii2TestAppTestUnit;

use Pantagruel74\Yii2TestAppTestHelpers\AbstractWebTest;
use yii\web\User;

class WebApplicationTest extends AbstractWebTest
{

    /**
     * @return array
     */
    protected function getConfig(): array
    {
        $ds = DIRECTORY_SEPARATOR;
        $config = include dirname(__DIR__) . $ds . 'config' . $ds . 'web.php';
        $config['controllerNamespace'] = 'Pantagruel74\Yii2TestAppStubs';
        $config['components']['user'] = ['class' => User::class];
        return $config;
    }

    protected function getRoute(): string
    {
        return 'test/test';
    }

    protected function getParams(): array
    {
        return [];
    }

    protected function assertAfterRunState(): void
    {
    }

    protected function assertOutput(string $content): void
    {
        $this->assertNotFalse(mb_stripos($content, 'web-console-test-successfull!'));
    }

    protected function assertStatusCode(int $statusCode): void
    {
    }

    protected function preRunScript(): void
    {
    }
}